package com.biquge.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wangmeng
 * @since 2021/12/11
 */
@Data
@Accessors(chain = true)
@TableName("novel_copy")
public class NovelCopy {

    @TableField("id")
    private Integer id;
    @TableField("copy_name")
    private String copyName;
    @TableField("novel_id")
    private Integer novelId;
    @TableField("chapter_num")
    private Integer chapterNum;
}
