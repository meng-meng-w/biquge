package com.biquge.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wangmeng
 * @since 2021/12/11
 */
@Data
@Accessors(chain = true)
@TableName("novel")
public class Novel {

    @TableField("id")
    private Integer id;
    @TableField("novel_name")
    private String novelName;
    @TableField("status")
    private String status;
    @TableField("latest_chapter")
    private String latestChapter;
    @TableField("chapter_num")
    private Integer chapterNum;
}
