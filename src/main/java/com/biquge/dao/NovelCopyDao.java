package com.biquge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.biquge.entity.NovelCopy;
import org.springframework.stereotype.Repository;

/**
 * @author wangmeng
 * @since 2021/12/11
 */
@Repository
public interface NovelCopyDao extends BaseMapper<NovelCopy> {
}
