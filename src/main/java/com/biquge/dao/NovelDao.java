package com.biquge.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.biquge.entity.Novel;
import org.springframework.stereotype.Repository;

/**
 * @author wangmeng
 * @since 2021/12/11
 */
@Repository
public interface NovelDao extends BaseMapper<Novel> {
}
