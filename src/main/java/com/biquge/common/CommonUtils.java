package com.biquge.common;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author wangmeng
 * @since 2021/12/11
 */
@Slf4j
public class CommonUtils {

    /**
     * 获取小说名
     *
     * @param url
     * @return
     */
    public static String getFromIndex(String url) {
        try {
            Document parse = Jsoup.connect(url).get();
            Elements elementsByTag = parse.getElementsByClass("con_top").first().getElementsByTag("a");
            String text = elementsByTag.get(1).text();
            log.info("获取到小说名称:{}", text);
            return text;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomException("获取小说名称失败!");
        }
    }
}
