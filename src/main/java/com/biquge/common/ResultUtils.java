package com.biquge.common;


import com.biquge.entity.CommonResult;

/**
 * 统一返回结果
 *
 * @author com.wangm
 * @since 2021/7/25
 */
public class ResultUtils {


    public static <T> CommonResult<T> success() {
        return new CommonResult<>(200, "操作成功!", null);
    }

    public static <T> CommonResult<T> success(String message) {
        return new CommonResult<>(200, message, null);
    }

    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<>(200, "操作成功!", data);
    }

    public static <T> CommonResult<T> success(T data, String message) {
        return new CommonResult<>(200, message, data);
    }

    public static <T> CommonResult<T> fail(String message) {
        return new CommonResult<>(400, message, null);
    }


}
