package com.biquge.common;

/**
 * 自定义异常
 *
 * @author com.wangm
 * @since 2021/7/25
 */
public class CustomException extends RuntimeException {

    public CustomException() {
        super();
    }

    public CustomException(String message) {
        super(message);
    }

}
