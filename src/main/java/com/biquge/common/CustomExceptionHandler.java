package com.biquge.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author wangmeng
 * @since 2021/12/11
 */
@ControllerAdvice
public class CustomExceptionHandler {



    @ExceptionHandler(value = CustomException.class)
    @ResponseBody
    public Object customException(CustomException e) {
        return ResultUtils.fail(e.getMessage());
    }

}
